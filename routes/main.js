/*
* Routes: Global
* Global router configurations that apply to the entire application.
*/

Router.configure({
  loadingTemplate: 'loading',
  notFoundTemplate: 'notFound',
  layoutTemplate: 'starterLayout'
});

Router.route('index', {
  path: '/',
  template: 'index',
  subscriptions: function(){
    return Meteor.subscribe('examplePublication');
    /* 
    return [
      Meteor.subscribe('examplePublication'),
      Meteor.subscribe('examplePublication2')
    ];
    */
  },
  onBeforeAction: function(){
    // Code to run before route goes here.
    this.next();
  }
});
