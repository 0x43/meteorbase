# Meteor Base

Meteor Base of some sorts :)

### Version
0.0.1

### Development

Open your favorite Terminal and run these commands.

Install Meteor:
```sh
curl https://install.meteor.com/ | sh
# May require sudo :)
curl https://install.meteor.com/ | sudo sh 
```

Runnig it:
```sh
git clone git@bitbucket.org:0x43/meteorbase.git
cd meteorbase
meteor
```

## Upstreaming / Proper forking:
Fork via Github/BitBucket

#### Setup
```sh
git clone git@bitbucket.org:[YOUR_REPO]/meteorbase.git
cd meteorbase
git remote add upstream git@bitbucket.org:0x43/meteorbase.git
```

#### Updating
```sh
git merge upstream/master
```

### Todo's

 - \*
 - %
 - infinity plus one


License
----

GNU General Public License v3 (GPL-3)
The GPLv3 allows others to copy, distribute and modify the software as long as they state what has been changed when, and ensure that any modifications are also licensed under the GPL. Software incorporating (via compiler) GPL-licensed code must also be made also available under the GPLv3 along with build & install instructions.

